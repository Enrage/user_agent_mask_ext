browser.runtime.onMessage.addListener((msg, sender, sendResponse) => {

  function headerChange(e) {
    for (let header of e.requestHeaders) {
      if (header.name.toLowerCase() === 'user-agent') header.value = msg.userAgent;
    }
    return { requestHeaders: e.requestHeaders };
  }
  
  if (msg.request === 'switchUserAgent') {

    browser.webRequest.onBeforeSendHeaders.addListener(
      headerChange,
      { urls: ["<all_urls>"] }, ['requestHeaders', 'blocking']
    );

    browser.storage.sync.set({userAgent: msg.userAgent, checkedInput: msg.checkedInput})
      .then(() => console.log(`Saved new UserAgent: ${msg.userAgent} & checkedInput: ${msg.checkedInput}`));

    browser.tabs.query({ active: true, currentWindow: true }, tabs => {
      browser.tabs.reload(tabs[0].id);
    });

    sendResponse({ status: 'Success', userAgent: msg.userAgent });
  }
});