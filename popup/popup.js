const listUserAgent = document.querySelectorAll('input[name="user-agent"]');
const arrayUA = Array.from(listUserAgent);
const userAgentDiv = document.querySelector('.user-agent');
const changeBtn = document.querySelector('.change-btn');

let currentUserAgent;
let myUserAgent;

const userAgentArr = [
	{
		id: 1,
		userAgent: 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; de-de) AppleWebKit/531.22.7 (KHTML, like Gecko) Version/4.0.5 Safari/531.22.7'
	},
	{
		id: 2,
		userAgent: 'Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; rev1.5; Windows NT 5.1; .NET CLR 1.1.4322)'
	},
	{
		id: 3,
		userAgent: 'Mozilla/5.0 (compatible; Konqueror/4.1; Linux) KHTML/4.1.3 (like Gecko) `Fedora`/4.1.3-3.fc10'
	}
];

browser.storage.sync.get('myUserAgent').then(response => {
  if (!response.myUserAgent) {
		myUserAgent = navigator.userAgent;
    browser.storage.sync.set({myUserAgent: myUserAgent})
      .then(() => console.log('Success saved myUserAgent'));
		userAgentDiv.innerText = myUserAgent;
  } else {
		myUserAgent = response.myUserAgent;
		browser.storage.sync.get(['userAgent', 'checkedInput']).then(res => {
			if (res.userAgent && res.checkedInput) {
				currentUserAgent = res.userAgent;
				let checked = document.querySelector(`input[value="${res.checkedInput}"]`);
				checked.checked = true;
			} else {
				currentUserAgent = myUserAgent;	
			}
			userAgentDiv.innerText = currentUserAgent;
		});
	}
});

function switchUserAgent() {
	let checkedUserAgent = arrayUA.filter(item => item.checked)[0].value;
	let userAgent = (checkedUserAgent !== '0') ? userAgentArr.filter(ua => ua.id === +checkedUserAgent)[0].userAgent : myUserAgent;

	browser.runtime.sendMessage({
		request: 'switchUserAgent',
		userAgent: userAgent,
		checkedInput: checkedUserAgent,
		myUserAgent: myUserAgent
	}, response => {
		if (response.status === 'Success') userAgentDiv.innerText = response.userAgent;
		else userAgentDiv.innerText = myUserAgent;
	});
}

changeBtn.addEventListener('click', switchUserAgent);